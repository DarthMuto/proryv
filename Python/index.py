import pymorphy2
from flask import Flask, Response, request
import json
from wiki_ru_wordnet import WikiWordnet


app = Flask(__name__)
morphy = pymorphy2.MorphAnalyzer()
wn = WikiWordnet()
pos_whitelist = {
        'NOUN',
        # 'ADJF',
        'ADJS',
        'COMP',
        'VERB',
        'INFN',
        'PRTF',
        'PRTS',
        'GRND',
        # 'ADVB',
        None,
}


@app.route('/')
def index():
    return 'it works'


def normal_form(word):
    if str(word).isnumeric():
        return None
    parsed = morphy.parse(word)[0]
    if parsed.tag.POS not in pos_whitelist:
        return None
    lemma = parsed.normal_form
    if morphy.parse(lemma)[0].tag.POS not in pos_whitelist:
        return None
    return lemma


def get_input():
    if not request.is_json:
        print('Non-JSON request')
        return None
    words = request.json
    if type(words) != list:
        print(words)
        print('Request body type is not list / array')
        return None
    return words


@app.route('/api/lemma', methods=['POST'])
def lemma():
    words = get_input()
    if words is None:
        return Response('[]', mimetype='application/json')
    result = [normal_form(word) for word in words]
    print('<<< OUT', result)
    return Response(json.dumps(result), mimetype='application/json')


@app.route('/api/syn', methods=['POST'])
def syn():
    words = get_input()
    if words is None:
        return Response('[]', mimetype='application/json')
    result = []
    for word in words:
        lemma = normal_form(word)
        if lemma:
            word = lemma
        word_synonyms = set()
        word_hypernyms = set()
        word_hyponyms = set()

        syn_sets = wn.get_synsets(word)
        for syn_set in syn_sets:
            for syn_word in syn_set.get_words():
                word_synonyms.add(syn_word.lemma())
            for hyper_syn_set in wn.get_hypernyms(syn_set):
                for syn_word in hyper_syn_set.get_words():
                    word_hypernyms.add(syn_word.lemma())
            for hypo_syn_set in wn.get_hyponyms(syn_set):
                for syn_word in hypo_syn_set.get_words():
                    word_hyponyms.add(syn_word.lemma())
        result.append({
            'word': word,
            'synonyms': list(word_synonyms),
            'hypernyms': list(word_hypernyms),
            'hyponyms': list(word_hyponyms),
        })
    return Response(json.dumps(result), mimetype='application/json')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
