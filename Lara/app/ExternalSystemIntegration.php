<?php


namespace App;


use App\Models\Appeal;

class ExternalSystemIntegration
{
    public function createRequest(Appeal $appeal) {
        $appeal->external_request_id = rand();
        $appeal->save();
        return $appeal->external_request_id;
    }
}
