<?php


namespace App;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class NlpClient
{
    const LEMMATIZE_URL = 'http://python:5000/api/lemma';
    const SYNONYMS_URL = 'http://python:5000/api/syn';

    const SPLIT_STR = 1;
    const RETURN_SET = 2;

    public function lemmatize($text, $flags = self::RETURN_SET | self::SPLIT_STR) {
        if (is_string($text) && ($flags & self::SPLIT_STR)) {
            $text = preg_split('#[^\w]+#u', $text);
        }
        $client = new Client();
        $responseJson = $client->post(self::LEMMATIZE_URL, [RequestOptions::JSON => $text])->getBody()->getContents();
        $response = json_decode($responseJson);
        if ($flags & self::RETURN_SET)
            $response = array_filter(array_unique($response));

        return $response;
    }

    public function synonyms($text) {
        $text = preg_split('#[^\w]+#u', $text);
        $client = new Client();
        $responseJson = $client->post(self::SYNONYMS_URL, [RequestOptions::JSON => $text])->getBody()->getContents();
        $response = json_decode($responseJson, true);
        $result = [];
        foreach ($response as $wordData) {
            $result = array_merge($result, [$wordData['word']], $wordData['synonyms']);
        }
        $result = array_filter(array_unique($result));

        return $result;
    }
}
