<?php

namespace App\Http\Controllers;

use App\ExternalSystemIntegration;
use App\Models\Appeal;
use App\Models\Tag;
use App\NlpClient;
use App\QuestionSearch;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OperatorShellController extends Controller
{
    /**
     * @var NlpClient
     */
    private $nlp;
    /**
     * @var QuestionSearch
     */
    private $search;
    /**
     * @var ExternalSystemIntegration
     */
    private $external;

    public function __construct(NlpClient $nlp, QuestionSearch $search, ExternalSystemIntegration $external)
    {
        $this->nlp = $nlp;
        $this->search = $search;
        $this->external = $external;
    }

    public function index() {
        $tagsLookup = Tag::getTree();
        // return view('operator-shell', compact('tagsLookup'));
        return view('operator-shell-v2', compact('tagsLookup'));
    }

    public function proposeTags(Request $request) {
        // $lemmas = $this->nlp->lemmatize($request->summary);
        $lemmas = $this->nlp->synonyms($request->summary);

        $proposedTags = Tag::whereHas('tag_keywords', function(Builder $builder) use ($lemmas) {
            $builder->whereIn('word', $lemmas);
        })->get();
        $proposedTagNames = $proposedTags->pluck('name');
        $proposedTagIds = $proposedTags->pluck('id');

        $questions = $this->search->findQuestionsByTagIds($proposedTagIds);

        return compact('proposedTagNames', 'questions');
    }

    public function questions(Request $request) {
        $tags = $request->tags;
        if (!$tags)
            return ['questions' => []];
        $questions = $this->search->findQuestionByTags($tags);
        return compact('questions');
    }

    public function createAppeal(Request $request) {
        $channel = ['channel_id' => 1];
        $appeal = Appeal::create($channel + $request->only(['address', 'summary', 'question_id', 'person_name', 'person_phone']));
        $local_id = $appeal->id;
        $external_id = null;
        if ($request->request_needed) {
            $external_id = $this->external->createRequest($appeal);
        }

        return compact('local_id', 'external_id');
    }
}



