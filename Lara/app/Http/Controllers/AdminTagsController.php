<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\NlpClient;
use Illuminate\Http\Request;

class AdminTagsController extends Controller
{
    /**
     * @var NlpClient
     */
    private $nlp;

    public function __construct(NlpClient $nlp)
    {
        $this->nlp = $nlp;
    }

    public function index() {
        return view('admin.tags-list', ['tags' => Tag::orderBy('name')->simplePaginate()]);
    }

    public function save($id, Request $request) {
        $tag = $id == 'new' ? new Tag() : Tag::findOrFail($id);

        if ($request->delete) {
            $tag->tag_keywords()->delete();
            $tag->question_tags()->delete();
            $tag->delete();
            return redirect()->route('admin-tags');
        }

        $tag->name = $request->name;
        $tag->save();
        $newKeywords = collect($this->nlp->lemmatize($request->keywords));
        $oldKeywords = $tag->tag_keywords->pluck('word');
        $tag->tag_keywords()->whereIn('word', $oldKeywords->diff($newKeywords))->delete();
        foreach ($newKeywords->diff($oldKeywords) as $word) {
            $tag->tag_keywords()->firstOrCreate(compact('word'));
        }

        return redirect()->route('admin-tags');
    }

    public function form($id) {
        $tag = $id == 'new' ? new Tag() : Tag::findOrFail($id);
        return view('admin.tags-edit', [
            'id' => $id,
            'name' => $tag->name,
            'keywords' => $tag->tag_keywords->pluck('word')->join("\n"),
        ]);
    }
}
