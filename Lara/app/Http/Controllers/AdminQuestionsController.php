<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;

class AdminQuestionsController extends Controller
{
    public function __construct()
    {
    }

    public function index() {
        $questions = Question::orderBy('header')->simplePaginate();
        return view('admin.questions-list', compact('questions'));
    }

    public function form($id) {
        $question = $id == 'new' ? new Question() : Question::findOrFail($id);
        return view('admin.questions-edit', compact('question', 'id'));
    }

    public function save($id, Request $request) {
        $question = $id == 'new' ? new Question() : Question::findOrFail($id);

        if ($request->delete) {
            $question->delete();
            return redirect()->route('admin-questions');
        }

        $question->header = $request->header;
        $question->answer = $request->answer;
        $question->description = $request->description;
        $question->extra_questions = $request->extra_questions;
        $question->save();

        return redirect()->route('admin-questions');
    }
}
