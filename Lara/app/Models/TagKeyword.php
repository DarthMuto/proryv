<?php

namespace App\Models;

/**
 * App\Models\TagKeyword
 *
 * @property int $id
 * @property int $tag_id
 * @property string $word
 * @property-read \App\Models\Tag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TagKeyword whereWord($value)
 * @mixin \Eloquent
 */
class TagKeyword extends \App\Models\Base\TagKeyword
{
	protected $fillable = [
		'tag_id',
		'word'
	];
}
