<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\QuestionTag
 *
 * @property int $id
 * @property int $question_id
 * @property int $tag_id
 * @property-read \App\Models\Question $question
 * @property-read \App\Models\Tag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\QuestionTag whereTagId($value)
 * @mixin \Eloquent
 */
class QuestionTag extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'question_id' => 'int',
		'tag_id' => 'int'
	];

	public function question()
	{
		return $this->belongsTo(\App\Models\Question::class);
	}

	public function tag()
	{
		return $this->belongsTo(\App\Models\Tag::class);
	}
}
