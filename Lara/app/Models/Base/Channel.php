<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\Channel
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appeal[] $appeals
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Channel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Channel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Channel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Channel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Channel whereName($value)
 * @mixin \Eloquent
 */
class Channel extends Eloquent
{
	public $timestamps = false;

	public function appeals()
	{
		return $this->hasMany(\App\Models\Appeal::class);
	}
}
