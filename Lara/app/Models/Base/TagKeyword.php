<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\TagKeyword
 *
 * @property int $id
 * @property int $tag_id
 * @property string $word
 * @property-read \App\Models\Tag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\TagKeyword whereWord($value)
 * @mixin \Eloquent
 */
class TagKeyword extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'tag_id' => 'int'
	];

	public function tag()
	{
		return $this->belongsTo(\App\Models\Tag::class);
	}
}
