<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\Tag
 *
 * @property int $id
 * @property int|null $parent_tag_id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $questions
 * @property-read \App\Models\Tag|null $tag
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TagKeyword[] $tag_keywords
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Tag whereParentTagId($value)
 * @mixin \Eloquent
 */
class Tag extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent_tag_id' => 'int'
	];

	public function tag()
	{
		return $this->belongsTo(\App\Models\Tag::class, 'parent_tag_id');
	}

	public function questions()
	{
		return $this->belongsToMany(\App\Models\Question::class, 'question_tags')
					->withPivot('id');
	}

	public function tag_keywords()
	{
		return $this->hasMany(\App\Models\TagKeyword::class);
	}

	public function tags()
	{
		return $this->hasMany(\App\Models\Tag::class, 'parent_tag_id');
	}
}
