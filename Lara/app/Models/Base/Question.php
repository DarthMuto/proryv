<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\Question
 *
 * @property int $id
 * @property string $header
 * @property string|null $description
 * @property string $extra_questions
 * @property string|null $answer
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appeal[] $appeals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Base\Question onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereExtraQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Base\Question withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Base\Question withoutTrashed()
 * @mixin \Eloquent
 */
class Question extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $timestamps = false;

	public function appeals()
	{
		return $this->hasMany(\App\Models\Appeal::class);
	}

	public function tags()
	{
		return $this->belongsToMany(\App\Models\Tag::class, 'question_tags')
					->withPivot('id');
	}
}
