<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 28 Jul 2019 08:21:10 +0000.
 */

namespace App\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * App\Models\Base\Appeal
 *
 * @property int $id
 * @property string $created_at
 * @property string $address
 * @property int $channel_id
 * @property string $summary
 * @property int $question_id
 * @property string $person_name
 * @property string $person_phone
 * @property string|null $external_request_id
 * @property-read \App\Models\Channel $channel
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereExternalRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal wherePersonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal wherePersonPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Appeal whereSummary($value)
 * @mixin \Eloquent
 */
class Appeal extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'channel_id' => 'int',
		'question_id' => 'int'
	];

	public function channel()
	{
		return $this->belongsTo(\App\Models\Channel::class);
	}

	public function question()
	{
		return $this->belongsTo(\App\Models\Question::class);
	}
}
