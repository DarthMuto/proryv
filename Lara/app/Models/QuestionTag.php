<?php

namespace App\Models;

/**
 * App\Models\QuestionTag
 *
 * @property int $id
 * @property int $question_id
 * @property int $tag_id
 * @property-read \App\Models\Question $question
 * @property-read \App\Models\Tag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionTag whereTagId($value)
 * @mixin \Eloquent
 */
class QuestionTag extends \App\Models\Base\QuestionTag
{
	protected $fillable = [
		'question_id',
		'tag_id'
	];
}
