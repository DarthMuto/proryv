<?php

namespace App\Models;

/**
 * App\Models\Appeal
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $created_at
 * @property string $address
 * @property int $channel_id
 * @property string $summary
 * @property int $question_id
 * @property string $person_name
 * @property string $person_phone
 * @property string|null $external_request_id
 * @property-read \App\Models\Channel $channel
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereChannelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereExternalRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal wherePersonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal wherePersonPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appeal whereSummary($value)
 * @mixin \Eloquent
 */
class Appeal extends \App\Models\Base\Appeal
{
	protected $fillable = [
		'address',
		'channel_id',
		'summary',
		'question_id',
        'person_name',
        'person_phone',
        'external_request_id',
	];

	public $timestamps = true;

	const UPDATED_AT = null;
}
