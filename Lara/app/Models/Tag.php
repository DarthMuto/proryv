<?php

namespace App\Models;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property int|null $parent_tag_id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QuestionTag[] $question_tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $questions
 * @property-read \App\Models\Tag|null $tag
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TagKeyword[] $tag_keywords
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereParentTagId($value)
 * @mixin \Eloquent
 */
class Tag extends \App\Models\Base\Tag
{
	protected $fillable = [
		'parent_tag_id',
		'name'
	];

	public static function getTree(self $parent = null) {
	    if ($parent)
	        $tags = $parent->tags;
        else
            $tags = self::whereNull('parent_tag_id')->with('tags', 'tags.tags', 'tags.tags.tags')->orderBy('name')->get();
        $result = [];
        foreach ($tags as $tag) {
            $result[$tag->name] = self::getTree($tag);
        }

        return $result;
    }

    public function tags() {
	    return parent::tags()->orderBy('name');
    }

    public function question_tags() {
	    return $this->hasMany(QuestionTag::class, 'tag_id', 'id');
    }
}
