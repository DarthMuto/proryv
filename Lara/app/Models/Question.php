<?php

namespace App\Models;

/**
 * App\Models\Question
 *
 * @property int $id
 * @property string $header
 * @property string|null $description
 * @property string $extra_questions
 * @property string|null $answer
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appeal[] $appeals
 * @property-read mixed $tags_list
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QuestionTag[] $question_tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereExtraQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereId($value)
 * @mixin \Eloquent
 */
class Question extends \App\Models\Base\Question
{
	protected $fillable = [
		'header',
		'description',
		'extra_questions',
		'answer',
        'tags_list',
	];

	protected $appends = ['tags_list'];

	public function getTagsListAttribute() {
	    return $this->tags()->select('name')->get()->pluck('name');
    }

	public function question_tags() {
	    return $this->hasMany(QuestionTag::class, 'question_id', 'id');
    }
}
