<?php

namespace App\Console\Commands;

use App\Console\Commands\Misc\TableRecord;
use App\Importer;
use App\Models\Question;
use App\Models\QuestionTag;
use App\Models\Tag;
use App\Models\TagKeyword;
use App\NlpClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ParseTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var Importer
     */
    private $importer;


    /**
     * Create a new command instance.
     *
     * @param Importer $importer
     */
    public function __construct(Importer $importer)
    {
        parent::__construct();
        $this->importer = $importer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->importer->truncateQuestions();
        $this->importer->importFile();
        $this->importer->createTagKeywords();
    }
}
