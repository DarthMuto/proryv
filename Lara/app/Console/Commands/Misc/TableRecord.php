<?php


namespace App\Console\Commands\Misc;


class TableRecord
{
    public $question;
    public $description;
    public $extraQuestions;
    public $answer;
    public $tag;

    public function __construct($array)
    {
        [$this->question, $_, $this->description, $this->extraQuestions, $_, $_, $a0, $a1] = $array;
        $this->answer = trim($a0 . $a1);

        if (preg_match('#Контекстный поиск «([^»]+)»#', $this->answer, $matches)) {
            $this->tag = $matches[1];
            $this->answer = str_replace($matches[0], '', $this->answer);
        }
    }

    public function isNewRecord() {
        return $this->question && $this->description && $this->extraQuestions && ($this->answer || $this->tag);
    }

    public function isEmpty() {
        return !$this->answer && !$this->extraQuestions && !$this->description && !$this->question;
    }

    public function add(TableRecord $other) {
        if ($other->question)
            $this->question .= "\n" . $other->question;
        if ($other->description)
            $this->description .= "\n" . $other->description;
        if ($other->extraQuestions)
            $this->extraQuestions .= "\n" . $this->extraQuestions;
        if ($other->answer)
            $this->answer .= "\n" . $other->answer;
    }
}
