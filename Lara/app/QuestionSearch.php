<?php


namespace App;


use App\Models\Question;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Builder;

class QuestionSearch
{
    public function findQuestionByTags($tagStrings) {
        $tagIds = Tag::whereIn('name', $tagStrings)->select('id')->get()->pluck('id');
        return $this->findQuestionsByTagIds($tagIds);
    }

    public function findQuestionsByTagIds($tagIds) {

        if (count($tagIds) == 0)
            return collect();

        $query = Question::select();
        foreach ($tagIds as $tagId) {
            $query->whereHas('question_tags', function (Builder $builder) use ($tagId) {
                $builder->where('tag_id', '=', $tagId);
            });
        }

        return $query->get();
    }
}
