<?php


namespace App;


use App\Console\Commands\Misc\TableRecord;
use App\Models\Appeal;
use App\Models\Question;
use App\Models\QuestionTag;
use App\Models\Tag;
use App\Models\TagKeyword;
use Illuminate\Support\Facades\DB;

class Importer
{
    const LEMMA_UNIQUENESS_CONSTRAINT = 0.06;

    /**
     * @var NlpClient
     */
    private $nlpClient;

    public function __construct(NlpClient $nlpClient)
    {
        $this->nlpClient = $nlpClient;
    }

    public function truncateQuestions() {
        TagKeyword::where('id', '>', 0)->delete();
        QuestionTag::where('id', '>', 0)->delete();
        Appeal::where('id', '>', 0)->delete();
        Question::where('id', '>', 0)->forceDelete();
        Tag::where('id', '>', 0)->delete();
        foreach (['tags', 'questions', 'question_tags', 'tag_keywords'] as $table)
            DB::select('alter table ' . $table . ' auto_increment = 1');
    }

    public function createTagKeywords($tags = null) {
        $tagData = [];
        if (!$tags)
            $tags = Tag::all();
        foreach ($tags as $tag) {
            $descriptions = [];
            foreach ($tag->questions as $question) {
                /** @var Question $question */
                $descriptions += $this->nlpClient->lemmatize($question->description);
            }
            $tagData[$tag->id] = array_unique($descriptions);
        }
        $tagsPerLemma = [];
        foreach ($tagData as $tagId => $lemmas)
            foreach ($lemmas as $lemma)
                $tagsPerLemma[$lemma][$tagId] = $tagId;

        foreach ($tagsPerLemma as $lemma => $lemmaTags) {
            if (count($lemmaTags) / $tags->count() > self::LEMMA_UNIQUENESS_CONSTRAINT)
                continue;
            foreach ($lemmaTags as $tagId)
                TagKeyword::firstOrCreate(['tag_id' => $tagId, 'word' => $lemma]);
        }
    }

    private function createRecord(TableRecord $record): Question {
        $question = Question::firstOrCreate([
            'answer' => $record->answer,
            'description' => $record->description,
            'extra_questions' => $record->extraQuestions,
            'header' => $record->question,
        ]);

        if ($record->tag) {
            $tag = Tag::firstOrCreate(['name' => $record->tag]);
            QuestionTag::create(['question_id' => $question->id, 'tag_id' => $tag->id]);
        }

        return $question;
    }

    public function importFile(): void
    {
        $fileName = 'table.csv';
        $fp = fopen($fileName, 'r');
        $i = -1;
        /** @var TableRecord $currentRecord */
        $currentRecord = null;
        while ($row = fgetcsv($fp, 0, ';', '"', '"')) {
            if (++$i < 2)
                continue;

            $record = new TableRecord($row);

            if ($record->isEmpty())
                continue;

            if ($record->isNewRecord()) {
                if ($currentRecord)
                    $this->createRecord($currentRecord);
                $currentRecord = $record;
            } else {
                $currentRecord->add($record);
            }

            if ($i >= 1380) {
                $this->createRecord($currentRecord);
                // format needs fixing
                break;
            }
        }
    }

}
