<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Support\Facades\Route;

/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('/', 'OperatorShellController@index')->name('oper-index');
Route::post('/propose-tags', 'OperatorShellController@proposeTags');
Route::post('/questions', 'OperatorShellController@questions');
Route::post('/appeal', 'OperatorShellController@createAppeal');


Route::get('/admin', function() { return redirect()->route('admin-questions'); });

Route::get('/admin/tags', 'AdminTagsController@index')->name('admin-tags');
Route::get('/admin/tags/edit/{id}', 'AdminTagsController@form')->name('admin-tags-edit');
Route::post('/admin/tags/edit/{id}', 'AdminTagsController@save');

Route::get('/admin/questions', 'AdminQuestionsController@index')->name('admin-questions');
Route::get('/admin/tags/edit/{id}', 'AdminQuestionsController@form')->name('admin-questions-edit');
Route::post('/admin/tags/edit/{id}', 'AdminQuestionsController@save');
