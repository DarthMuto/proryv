@extends('layout')

@section('head')
    <link rel="stylesheet" href="/css/operator-shell.css"/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/operator-shell.js"></script>
@endsection

@section('body')
<div style="margin: 50px auto; width: 800px;" class="oper-shell-body">

    <input type="hidden" name="csrf" id="csrf" value="{{csrf_token()}}"/>

    <div id="saved-appeal" style="display: none">
        Обращение сохранено, id = <span id="saved-appeal-id"></span>
    </div>
    <div id="saved-request" style="display: none">
        Заявка создана, id = <span id="saved-request-id"></span>
    </div>

    <div>
        Имя: <input type="text" name="name" id="name"/>
    </div>
    <div>
        Адрес: <input type="text" name="address" id="address"/>
    </div>
    <div>
        Телефон: <input type="tel" name="phone" id="phone"/>
    </div>

    <div>
        Описание проблемы:<br/>
        <textarea name="summary" id="summary" cols="40" rows="5"></textarea>
    </div>

    <div style="display: none" id="selected-question" class="selected-question">
        <div id="selected-question-header"></div>
        <div id="selected-question-description"></div>
        <div id="selected-question-extra-questions"></div>
        <div id="selected-question-answer"></div>
        <div>
            <input type="button" id="selected-question-save" value="Вопрос решен"/>
            <input type="button" id="selected-question-create-request" value="Создать запрос"/>
            <input type="button" id="selected-question-cancel" value="Отмена"/>
        </div>
    </div>

    <div class="tags-list" style="display: none">
        Ищем по:
        <ul id="assigned-tags"></ul>
    </div>

    <div id="tags-not-found" style="display: none">
        Не получилось определить теги автоматически.
    </div>

    <div class="tags-list">
        Все тэги:
        <ul id="all-tags"></ul>
    </div>

    <div class="questions">
        <div id="questions-not-found" style="display: none">По заданным тегам ничего не нашлось, попробуйте облегчить критерии поиска</div>
        <ul id="questions"></ul>
    </div>

    <script>
        const tagsLookup = @json($tagsLookup);
    </script>
</div>
@endsection

