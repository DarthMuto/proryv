<?php
/**
 * @var \App\Models\Tag[]|\Illuminate\Support\Collection $tags
 */
?>
@extends('admin.layout')
@section('body')

    <a href="{{route('admin-tags-edit', ['id' => 'new'])}}">Создать тэг</a>
    {{$tags->links()}}
    @foreach($tags as $tag)
        <div>
            <a href="{{route('admin-tags-edit', ['id' => $tag->id])}}">{{$tag->id}}, {{$tag->name}} (вопросов: {{$tag->questions()->count()}})</a>
        </div>
    @endforeach
    {{$tags->links()}}

@endsection
