<?php
/**
 * @var string $id
 * @var string $name
 * @var string $keywords
 */
?>
@extends('admin.layout')
@section('body')
    <form action="{{route('admin-tags-edit', ['id' => $id])}}" method="post">
        @csrf
        <div>
            Тэг:
            <input type="text" name="name" value="{{$name}}"/>
        </div>
        <div>
            Ключевые слова:<br/>
            <textarea name="keywords" cols="30" rows="5">{{$keywords}}</textarea>
        </div>
        @if ($id != 'new')
            <div>
                <input type="checkbox" name="delete"/> Удалить
            </div>
        @endif
        <div>
            <input type="submit" value="Сохранить"/>
        </div>
    </form>
@endsection
