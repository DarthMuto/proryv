<?php
/**
 * @var \App\Models\Question $question
 * @var string $id
 */
?>
@extends('admin.layout')
@section('body')
    <form action="{{route('admin-questions-edit', ['id' => $id])}}" method="post">
        @csrf
        <div>
            Заголовок:<br/>
            <input type="text" value="{{$question->header}}" name="header"/>
        </div>
        <div>
            Описание:<br/>
            <textarea name="description" cols="20" rows="5">{{$question->description}}</textarea>
        </div>
        <div>
            Дополнительные вопросы:<br/>
            <textarea name="extra_questions" cols="20" rows="5">{{$question->extra_questions}}</textarea>
        </div>
        <div>
            Комментарий / ответ:<br/>
            <textarea name="answer" cols="20" rows="5">{{$question->answer}}</textarea>
        </div>
        @if ($id != 'new')
        <div>
            <input type="checkbox" name="delete"/> Удалить
        </div>
        @endif
        <div>
            TODO: TAGS
        </div>
        <div>
            <input type="submit" value="Сохранить"/>
        </div>
    </form>
@endsection
