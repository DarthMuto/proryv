<?php
/**
 * @var \App\Models\Question[]|\Illuminate\Support\Collection $questions
 */
?>
@extends('admin.layout')
@section('body')
    <div>
        <a href="{{route('admin-questions-edit', ['id' => 'new'])}}">Создать вопрос</a>
    </div>
    {{$questions->links()}}
    @foreach ($questions as $question)
        <div><a href="{{route('admin-questions-edit', ['id' => $question->id])}}">
            {{$question->id}}, {{$question->header}}
        </a></div>
    @endforeach
    {{$questions->links()}}
    <div>
        <a href="{{route('admin-questions-edit', ['id' => 'new'])}}">Создать вопрос</a>
    </div>
@endsection
