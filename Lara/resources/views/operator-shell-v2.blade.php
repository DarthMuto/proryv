<!DOCTYPE HTML>
<!--
	Overflow by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>ЕДЦ ЖКХ</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="/operator-shell-v2/assets/css/main.css" />
    <link rel="stylesheet" href="/css/operator-shell-v2.css"/>
    <noscript><link rel="stylesheet" href="/operator-shell-v2/assets/css/noscript.css" /></noscript>
</head>
<body class="is-preload">
<!-- Header -->
<section id="header">
    <header>
        <h1>ЕДЦ ЖКХ</h1>
        <p>База знаний</p>
    </header>
    <footer>
        <img src="/operator-shell-v2/images/CP.png" width="100" height="100" alt="" />
    </footer>
</section>
<!-- Contact -->
<article class="container box style3">
    <header>
        <h2>Единый диспетчерский центр</h2>
        <p>Цифровой Помощник</p>
    </header>
    <form>

        <input type="hidden" name="csrf" id="csrf" value="{{csrf_token()}}"/>

        <div class="row gtr-50">
            <div class="col-6 col-12-mobile"><input type="text" class="text" name="name" placeholder="Имя заявителя" id="name"/></div>
            <div class="col-6 col-12-mobile"><input type="text" class="text" name="phone" placeholder="Телефон заявителя" id="phone"/></div>
            <div class="col-12 col-12-mobile"><input type="text" class="text" name="address" placeholder="Адрес заявителя" id="address"/></div>
            <div class="col-12">
                <textarea name="message" placeholder="Текст обращения" id="summary"></textarea>
            </div>
            <div class="col-12">
                <div id="assigned-tags-message">Теги не выбраны</div>
                <div id="assigned-tags"></div>
            </div>

            <p id="all-tags"></p>
            <!-- div class="col-12"><input type="text" class="text" name="tags" placeholder="Теги" /></div -->
            <!-- div class="col-12">
                <textarea name="result" placeholder="Решение"></textarea>
            </div -->

            <div class="questions">
                <div id="questions-not-found" style="display: none">По заданным тегам ничего не нашлось, попробуйте облегчить критерии поиска</div>
                <ul id="questions"></ul>
            </div>

            <div style="display: none" id="selected-question" class="selected-question">
                <div id="selected-question-header"></div>
                <div id="selected-question-description"></div>
                <div id="selected-question-extra-questions"></div>
                <div id="selected-question-answer"></div>
            </div>

            <div id="saved-appeal" style="display: none">Обращение сохранено</div>

            <div class="col-12">
                <ul class="actions">
                    <li><input type="button" value="Отправить" id="selected-question-save"/></li>
                    <li><input type="button" value="Сбросить" id="selected-question-cancel"/></li>
                </ul>
            </div>
        </div>
    </form>
</article>

<section id="footer">
    <ul class="icons">
        <li><a href="#" id="selected-question-create-request">ЕДЦ</a></li>
    </ul>
    <div class="copyright">
        <ul class="menu">
            <li>&copy; Digital Russia</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
    </div>
</section>

<!-- Scripts -->
<script>
    const tagsLookup = @json($tagsLookup);
</script>
<script src="/operator-shell-v2/assets/js/jquery.min.js"></script>
<script src="/operator-shell-v2/assets/js/jquery.scrolly.min.js"></script>
<script src="/operator-shell-v2/assets/js/jquery.poptrox.min.js"></script>
<script src="/operator-shell-v2/assets/js/browser.min.js"></script>
<script src="/operator-shell-v2/assets/js/breakpoints.min.js"></script>
<script src="/operator-shell-v2/assets/js/util.js"></script>
<script src="/operator-shell-v2/assets/js/main.js"></script>
<script src="/js/operator-shell.js"></script>
</body>
</html>
