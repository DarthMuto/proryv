'use strict';

$(() => {

    const
        tagProposalRequestTimeoutMilliseconds = 1000,
        $summary = $('#summary'),
        $phone = $('#phone'),
        $address = $('#address'),
        $name = $('#name'),
        $assignedTags = $('#assigned-tags'),
        $proposedTags = $('#proposed-tags'),
        $allTags = $('#all-tags'),
        $questions = $('#questions'),
        _token = $('#csrf').val(),
        $selectedQuestion = $('#selected-question'),
        $selectedQuestionHeader = $('#selected-question-header'),
        $selectedQuestionDescription = $('#selected-question-description'),
        $selectedQuestionExtra = $('#selected-question-extra-questions'),
        $selectedQuestionAnswer = $('#selected-question-answer'),
        $saveButton = $('#selected-question-save'),
        $createRequestButton = $('#selected-question-create-request'),
        $cancelButton = $('#selected-question-cancel'),
        $savedAppeal = $('#saved-appeal'),
        $savedAppealId = $('#saved-appeal-id'),
        $savedRequest = $('#saved-request'),
        $savedRequestId = $('#saved-request-id'),
        $questionsNotFound = $('#questions-not-found'),
        $assignedTagsMessage = $('#assigned-tags-message');

    const tags = {
        assigned: [],
        proposed: [],
    };

    let
        tagProposalRequestTimeout = null,
        selectedQuestionId = null,
        wordsCount = 0,
        lastRequestSummary = null;

    const reloadQuestions = () => {
        $.post('/questions', {tags: tags.assigned, _token}, res => updateQuestions(res.questions), 'json');
    };

    const createTag = tag => $(`<a href="#" data-tag="${tag}">#${tag} </a>`);

    const tagSelector = tag => `a[data-tag="${tag}"]`;

    const assignedTagMessages = {
        notAssigned: 'Теги не выбраны',
        notFound: 'Не удалось определить теги автоматически',
    };
    const setAssignedTagsMessage = msg => {
        if (msg) {
            $assignedTagsMessage.html(msg);
            $allTags.show();
        } else {
            $assignedTagsMessage.html('');
            $allTags.hide();
        }
    };

    // Moves tag from 'all' to 'assigned' list
    const addTag = tag => {
        const $tag = createTag(tag);
        $tag.click(() => {
            removeTag(tag);
            reloadQuestions();
            return false;
        });
        $allTags.find(tagSelector(tag)).remove();
        if (!$assignedTags.find(tagSelector(tag)).length) {
            $assignedTags.append($tag);
            if (!tags.assigned.includes(tag))
                tags.assigned.push(tag);
        }
        setAssignedTagsMessage(null);
    };

    // Moves tag from 'assigned' to 'all' list
    const removeTag = tag => {
        const $tag = createTag(tag);
        $tag.click(e => {
            if (!$(e.target).hasClass('disabled')) {
                addTag(tag);
                reloadQuestions();
            }
            return false;
        });
        $assignedTags.find(tagSelector(tag)).remove();
        tags.assigned = tags.assigned.filter(t => t !== tag);
        if (!$allTags.find(tagSelector(tag)).length)
            $allTags.append($tag);
        if (!tags.assigned.length)
            setAssignedTagsMessage(assignedTagMessages.notAssigned);
    };

    // Refreshes tag sections on page according to received data
    const updateProposedTags = tagsList => {
        tags.proposed = tagsList;
        tagsList.forEach(addTag);
        console.log(tags);
        if (!tags.proposed.length && !tags.assigned.length)
            setAssignedTagsMessage(assignedTagMessages.notFound);
    };

    // Sets clicked question as active
    const selectQuestion = question => {
        if (!question) {
            $selectedQuestion.hide();
            selectedQuestionId = null;
            return;
        }
        $selectedQuestionHeader.html(question.header.trim().replace('\n', '<br/>'));
        $selectedQuestionDescription.html(question.description.trim().replace('\n', '<br/>'));
        $selectedQuestionExtra.html(question.extra_questions.trim().replace('\n', '<br/>'));
        $selectedQuestionAnswer.html(question.answer.trim().replace('\n', '<br/>'));
        selectedQuestionId = question.id;
        $selectedQuestion.show();
        return false;
    };

    // Disables / enables tags in 'all tags' list depending on tags for current questions list
    const disableTagsExcept = tags => {
        $allTags.find('a').each((_, a) => {
            const $a = $(a),
                tag = $a.attr('data-tag');
            if (tags.includes(tag))
                $a.removeClass('disabled');
            else
                $a.addClass('disabled');
        });
    };

    // Updates questions section on page, invoked after questions data is received
    const updateQuestions = questions => {
        if (questions.length)
            $questionsNotFound.hide();
        $questions.empty();
        const questionTags = [];
        questions.forEach(q => {
            const $tags = q.tags_list.map(tag => {
                const $tag = createTag(tag);
                questionTags.push(tag);
                $tag.click(() => { addTag(tag); reloadQuestions(); return false; });
                return $tag;
            });
            const $question = $(`<li><a href="#">Выбрать</a>: ${q.header} </li>`);
            $question.find('a').click(() => selectQuestion(q));
            $tags.forEach($tag => $question.append($tag));
            $questions.append($question);
        });
        if (tags.assigned.length)
            disableTagsExcept(questionTags);
        else
            $allTags.find('.disabled').removeClass('disabled');
    };

    // Requests data for possible tags according to problem summary
    const proposeTags = (cutTail = false) => {
        let summary = $summary.val();
        const nonWord = /[^\p{L}]+/u,
            summaryWords = summary.split(nonWord);

        if (cutTail) {
            summaryWords.pop();
            summary = summaryWords.join(' ');
        }

        if (summary === lastRequestSummary)
            return;
        lastRequestSummary = summary;

        $.post('/propose-tags', {summary, _token}, res => {
            updateProposedTags(res.proposedTagNames);
            updateQuestions(res.questions);
            if (tags.assigned.length && !res.questions.length)
                $questionsNotFound.show();
        }, 'json');
    };

    // Saves appeal, creates request in external system if needed
    const saveData = request_needed => {

        if (!selectedQuestionId)
            return false;

        for (let $input of [$name, $address, $phone, $summary]) {
            if (!$input.val()) {
                alert('Не заполнено: ' + $input.attr('placeholder'));
                $input.focus();
                return false;
            }
        }

        request_needed = request_needed ? 1 : 0;
        const data = {
            address: $address.val(),
            summary: $summary.val(),
            question_id: selectedQuestionId,
            person_name: $name.val(),
            person_phone: $phone.val(),
            request_needed,
            _token,
        };
        $.post('/appeal', data, ({local_id, external_id}) => {
            if (local_id) {
                $savedAppealId.html(local_id);
                $savedAppeal.show();
                setTimeout(() => $savedAppeal.hide(), 5000);
            }
            if (external_id) {
                $savedRequestId.html(external_id);
                $savedRequest.show();
                setTimeout(() => $savedRequest.hide(), 5000);
            }
            resetTags();
            selectQuestion(null);
            updateQuestions([]);
            $questionsNotFound.hide();
            $address.add($name).add($phone).add($summary).val('');
        }, 'json');
        return false;
    };

    // Reset tag lists (on init and after successful submit)
    const resetTags = () => {
        tags.assigned = [];
        tags.proposed = [];
        $assignedTags.empty();
        $proposedTags.empty();
        $allTags.empty();
        for (let tag in tagsLookup)
            removeTag(tag);
    };

    // {{{ Init
    $summary.keyup(() => {

        // Fires request after user finished typing a word
        const words = $summary.val();
        if (words.length >= wordsCount + 2) {
            wordsCount = words.length + 1;
            proposeTags(true);
        }

        // Fires request in some time after user stops typing
        if (tagProposalRequestTimeout)
            clearTimeout(tagProposalRequestTimeout);
        tagProposalRequestTimeout = setTimeout(proposeTags, tagProposalRequestTimeoutMilliseconds);
    });

    $summary.blur(() => {
        if (!$summary.val())
            return;
        if (tagProposalRequestTimeout)
            clearTimeout(tagProposalRequestTimeout);
        proposeTags();
    });

    $saveButton.click(() => saveData(false));
    $createRequestButton.click(() => saveData(true));
    $cancelButton.click(() => selectQuestion(null));
    resetTags();
    setAssignedTagsMessage(assignedTagMessages.notAssigned);
    // }}}
});
